package application;
//import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;


public class Controleur2 {
	@FXML
	Label scoreJ1;
	@FXML
	Label scoreJ2;
	@FXML
	ImageView grille;
	@FXML
	GridPane gp;

	int scoreJ1Int;
	int scoreJ2Int;
	String pseudo;

	Joueur joueur1;
	Pioche maPioche;
	Plateau monPlateau;

	public void setPseudo(String pseudos) {
		//Affichage du pseudo, hérité du premier controleur
		pseudo = pseudos;
		scoreJ1.setText(pseudo + " : " + scoreJ1Int);
	}
	public void initialize() {
		gp.setGridLinesVisible(true);

		//}


		scoreJ1.setText(pseudo + " : " + scoreJ1Int);

	}

	public void Démarrer(ActionEvent e) {
		Joueur joueur1 = new Joueur("joueur1");
		Pioche maPioche = new Pioche ("Ma pioche");
		Plateau monPlateau = new Plateau("Mon plateau",15);
		Biblio laBiblio = new Biblio("Dictionnaire.txt");
//		 Remplir le chevalet
		joueur1.piocher(maPioche);
		joueur1.afficherJeusimple();
//		while (joueur1.encoreLettres()){
//			// Jouer
			joueur1.jouer(monPlateau, laBiblio);
			joueur1.afficherJeusimple();
			joueur1.viderChevalet();
	}
	}
