package application;

import java.util.ArrayList;

public class Plateau {
	// Attributs
	String nom;
	// case normale : caseSpecial = 0
	// case lettre compte double : caseSpecial = 12
	// case lettre compte triple : caseSpecial = 13
	// case mot compte double : caseSpecial = 22
	// case mot compte triple : caseSpecial = 23
	int[][] tableauScore = { { 23, 0, 0, 12, 0, 0, 0, 23, 0, 0, 0, 12, 0, 0, 23 },
			{ 0, 22, 0, 0, 0, 13, 0, 0, 0, 13, 0, 0, 0, 22, 0 }, { 0, 0, 22, 0, 0, 0, 12, 0, 12, 0, 0, 0, 22, 0, 0 },
			{ 12, 0, 0, 22, 0, 0, 0, 12, 0, 0, 0, 22, 0, 0, 12 }, { 0, 0, 0, 0, 22, 0, 0, 0, 0, 0, 22, 0, 0, 0, 0 },
			{ 0, 13, 0, 0, 0, 13, 0, 0, 0, 13, 0, 0, 0, 13, 0 }, { 0, 0, 12, 0, 0, 0, 12, 0, 12, 0, 0, 0, 12, 0, 0 },
			{ 23, 0, 0, 12, 0, 0, 0, 22, 0, 0, 0, 12, 0, 0, 23 }, { 0, 0, 12, 0, 0, 0, 12, 0, 12, 0, 0, 0, 12, 0, 0 },
			{ 0, 13, 0, 0, 0, 13, 0, 0, 0, 13, 0, 0, 0, 13, 0 }, { 0, 0, 0, 0, 22, 0, 0, 0, 0, 0, 22, 0, 0, 0, 0 },
			{ 12, 0, 0, 22, 0, 0, 0, 12, 0, 0, 0, 22, 0, 0, 12 }, { 0, 0, 22, 0, 0, 0, 12, 0, 12, 0, 0, 0, 22, 0, 0 },
			{ 0, 22, 0, 0, 0, 13, 0, 0, 0, 13, 0, 0, 0, 22, 0 }, { 23, 0, 0, 12, 0, 0, 0, 23, 0, 0, 0, 12, 0, 0, 23 } };
	Lettre[][] tableauLettre;
	// case = 0 : case vide
	// case = 1 : case en cours de remplissage
	// case = 2 : case remplie
	int[][] dispo;

	// Constructeur
	public Plateau(String nm, int taille) {
		this.nom = nm;
		this.tableauLettre = new Lettre[taille][taille];
		this.dispo = new int[taille][taille];
	}

	// Méthodes

	// Vérifier que la case n'est pas déjà occupée
	public boolean caseDejaOccupee(Lettre aVerifier) {
		if (dispo[aVerifier.posLigne][aVerifier.posLigne] == 0) {
			dispo[aVerifier.posLigne][aVerifier.posLigne] = 1;
			return true;
		} else
			return false;
	}

	// Vérifier que les lettres sont bien sur la même ligne ou colonne
	public int[] memeLigneColonne(ArrayList<Lettre> proposition) {
		// Les valeurs suivantes vont déterminer les encadrements des lettres
		// Si l'encadrement n'est pas sur une ligne our sur une colonne, alors
		// les lettres ne sont pas bien plac�es
		int minLigne = 16, maxLigne = -1, minColonne = 16, maxColonne = -1;
		int[] valeursCases = { minLigne, maxLigne, minColonne, maxColonne };
		for (int i = 0; i < proposition.size(); i++) {
			dispo[proposition.get(i).posLigne][proposition.get(i).posColonne] = 2;
			if (proposition.get(i).posLigne < minLigne) {
				minLigne = proposition.get(i).posLigne;
			}
			if (proposition.get(i).posLigne > maxLigne) {
				maxLigne = proposition.get(i).posLigne;
			}
			if (proposition.get(i).posColonne < minColonne) {
				minColonne = proposition.get(i).posColonne;
			}
			if (proposition.get(i).posColonne > maxColonne) {
				maxColonne = proposition.get(i).posColonne;
			}
		}
		if (minColonne == maxColonne) {
			return valeursCases;
		} else
			return null;

	}

	// Avoir la longueur du mot en vérifiant des deux côtés
	public void longueurMot(int[] valeursCases) {
		// valeuresCases[0] = minLignes
		// valeuresCases[1] = maxLignes
		// valeuresCases[2] = minColonne
		// valeuresCases[3] = maxColonne
		if (valeursCases[0] == valeursCases[1]) {
			while (dispo[valeursCases[0]][valeursCases[2]] != 0) {
				if (dispo[valeursCases[0] - 1][valeursCases[2]] == 2) {
					// La case est occupée, donc il faut compter la lettre
					// dessus dans le mot
					valeursCases[2] = valeursCases[2] - 1;
				}
			}
			while (dispo[valeursCases[0]][valeursCases[3]] != 0) {
				if (dispo[valeursCases[0] + 1][valeursCases[3]] == 2) {
					// La case est occupée, donc il faut compter la lettre
					// dessus dans le mot
					valeursCases[3] = valeursCases[3] + 1;
				}
			}

			// Puisqu'on sait que les lettres sont sur la même ligne ou même
			// colonne, et qu'on vient de vérifier qu'elles ne sont pas sur la
			// même ligne, elles sont forcément sur la même colonne
		} else {
			while (dispo[valeursCases[0]][valeursCases[2]] != 0) {
				if (dispo[valeursCases[0] - 1][valeursCases[2]] == 2) {
					// La case est occupée, donc il faut compter la lettre
					// dessus dans le mot
					valeursCases[0] = valeursCases[0] - 1;
				}
			}
			while (dispo[valeursCases[1]][valeursCases[2]] != 0) {
				if (dispo[valeursCases[1] + 1][valeursCases[2]] == 2) {
					// La case est occupée, donc il faut compter la lettre
					// dessus dans le mot
					valeursCases[1] = valeursCases[1] + 1;
				}
			}
		}
	}

	// V�rifie qu'il ny ait pas de blancs entre deux lettres
	public boolean motComplet(int[] valeursCases) {
		boolean motComplet = true;
		// valeuresCases[0] = minLignes
		// valeuresCases[1] = maxLignes
		// valeuresCases[2] = minColonne
		// valeuresCases[3] = maxColonne
		if (valeursCases[0] == valeursCases[1]) {
			for (int k = valeursCases[2]; k <= valeursCases[3]; k++) {
				if (dispo[valeursCases[0]][k] == 0) {
					motComplet = false;
				}
			}
		} else {
			for (int k = valeursCases[0]; k <= valeursCases[1]; k++) {
				if (dispo[k][valeursCases[2]] == 0) {
					motComplet = false;
				}
			}
		}
		return motComplet;
	}

	// Avoir tous les mots cr��s et v�rifier qu'ils fonctionnent
	public boolean motNom(int[] valeursCases, Biblio laBiblio) {
		String motNom = "";
		String motNom2 = "";
		// valeureCases[0] = minLignes
		// valeursCases[1] = maxLignes
		// valeursCases[2] = minColonne
		// valeursCases[3] = maxColonne
		if (valeursCases[0] == valeursCases[1]) {
			for (int k = valeursCases[2]; k <= valeursCases[3]; k++) {
				// Obenir le mot principal
				motNom = motNom + tableauLettre[valeursCases[0]][k].nom;
				// Obtenir les mots qui croisent
				if (dispo[valeursCases[0] - 1][k] == 2) {
					int[] valeursCases2 = { valeursCases[0] - 1, valeursCases[0], k, k };
					longueurMot(valeursCases2);
					for (int i = valeursCases2[0]; i <= valeursCases2[1]; i++) {
						// Obenir le mot principal
						motNom2 = motNom2 + tableauLettre[i][valeursCases[2]].nom;
					}
					if (laBiblio.contientLeMot(motNom2) == false) {
						return false;
					}
				}
				if (dispo[valeursCases[0] + 1][k] == 2) {
					int[] valeursCases3 = { valeursCases[0], valeursCases[0] + 1, k, k };
					longueurMot(valeursCases3);
					for (int i = valeursCases3[0]; i <= valeursCases3[1]; i++) {
						// Obenir le mot principal
						motNom2 = motNom2 + tableauLettre[i][valeursCases[2]].nom;
					}
					if (laBiblio.contientLeMot(motNom2) == false) {
						return false;
					}
				}

			}
			if (laBiblio.contientLeMot(motNom) == false) {
				return false;
			} else
				return true;
		} else {
			for (int k = valeursCases[0]; k <= valeursCases[1]; k++) {
				// Obenir le mot principal
				motNom = motNom + tableauLettre[k][valeursCases[2]].nom;
				// Obtenir les mots qui croisent
				if (dispo[k][valeursCases[2] - 1] == 2) {
					int[] valeursCases2 = { k, k, valeursCases[2] - 1, valeursCases[2] };
					longueurMot(valeursCases2);
					for (int i = valeursCases2[2]; i <= valeursCases2[3]; i++) {
						motNom2 = motNom2 + tableauLettre[k][i].nom;
					}
					if (laBiblio.contientLeMot(motNom2) == false) {
						return false;
					}
				}
				if (dispo[k][valeursCases[2] + 1] == 2) {
					int[] valeursCases3 = { k, k, valeursCases[2], valeursCases[2] + 1 };
					longueurMot(valeursCases3);
					for (int i = valeursCases3[0]; i <= valeursCases3[1]; i++) {
						// Obenir le mot principal
						motNom2 = motNom2 + tableauLettre[k][i].nom;
					}
					if (laBiblio.contientLeMot(motNom2) == false) {
						return false;
					}
				}
			}
			if (laBiblio.contientLeMot(motNom) == false) {
				return false;
			} else
				return true;
		}

	}

	// Accpeter la proposition de l'utilisateur
	public void accepterLettres(ArrayList<Lettre> proposition){
		for (int k = 0; k < proposition.size(); k++){
			dispo[proposition.get(k).posLigne][proposition.get(k).posColonne] = 2;
			tableauLettre[proposition.get(k).posLigne][proposition.get(k).posColonne] = proposition.get(k);
		}
	}
}
