package application;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

public class Biblio {

	// Ensemble de mots du dictionnaire (initialis? a? 1.25 fois le nombre
	// d'éléments initiaux)
	HashSet<String> mots = new HashSet<>(420000);

	/**
	 * Constructeur: il remplit l'ensemble avec les mots lus dans le
	 * dictionnaire fichier.
	 *
	 * @param fileName
	 *            Chemin du fichier dans le projet
	 * @return
	 */
	public Biblio(String fileName) {
		try { // Lecture du fichier
				// ouverture du fichier
			BufferedReader reader = new BufferedReader(
					new BufferedReader ((new FileReader("Dictionnaire.txt"))));

			String ligne; // ligne courante
			while ((ligne = reader.readLine()) != null) { // jusqu'? atteindre
															// la fin
				mots.add(ligne); // ajouter ? l'ensemble
			}
			System.out.println("Le dictionnaire contient " + mots.size() + " mots.");

			reader.close(); // fermeture du fichier
		} catch (IOException e) {
			System.err.println("Erreur de lecture");
		}
	}

	/**
	 * V�rifie si le mot est contenu dans le dictionnaire. Le mot est
	 * pr�alablement transform? en minuscule pour la recherche.
	 *
	 * @param mot
	 *            Mot à vérifier (majuscule ou minuscule)
	 * @return Vrai si le mot se trouve dans le dictionnaire
	 */
	boolean contientLeMot(String mot) {
		return mots.contains(mot.toLowerCase());
	}
}
