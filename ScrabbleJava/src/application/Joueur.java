package application;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

public class Joueur {
	Scanner sc = new Scanner(System.in);
	// Attributs
	String nom;
	int score;
	ArrayList<Lettre> chevalet;
	ArrayList<Lettre> proposition;
	@FXML
	GridPane gp;
	// Constructeur
	public Joueur(String nm) {
		this.nom = nm;
		this.score = 0;
		this.chevalet = new ArrayList<Lettre>();
		this.proposition = new ArrayList<Lettre>();
	}

	// M騁hodes

	// Afficher le jeu de l'utilisateur
	public void afficherJeu() {
		for (int k = 0; k < this.chevalet.size(); k++) {
			this.chevalet.get(k).afficherLettre();
		}
	}

	// Afficher le jeu de l'utilisateur sous forme compacte
	public void afficherJeusimple() {
		if (encoreLettres()) {
			for (int k = 0; k < this.chevalet.size() - 1; k++) {
				System.out.print(this.chevalet.get(k).nom + ", ");
			}
			System.out.println(this.chevalet.get(this.chevalet.size() - 1).nom);
		} else
			System.out.println("Le chevalet est vide.");
	}

	// V駻ification s'il reste encore des lettres
	public boolean encoreLettres() {
		if (this.chevalet.size() != 0) {
			return true;
		} else
			return false;
	}

	// Vider le chevalet
	public void viderChevalet() {
		this.chevalet.clear();
	}


	  // Le joueur pose ses lettres en pr駸isant leurs positions public
	  ArrayList<Lettre> placerLettre() { 
		  System.out.println("1er : num駻o de la lettre sur le chevalet."); 
		  System.out.println("2em : num駻o de la colonne."); 
		  System.out.println("3em : num駻o de la ligne."); 
		  System.out.println("Tapez un chiffre n馮atif quand vous aurez fini."); 
		  int numLettre = 0;
	  numLettre = sc.nextInt(); 
	  while (numLettre >= 0) { 
		  int posiColonne =sc.nextInt(); 
		  int posiLigne = sc.nextInt();
	  this.chevalet.get(numLettre).posColonne = posiColonne;
	 this.chevalet.get(numLettre).posLigne = posiLigne;
	  this.proposition.add(this.chevalet.get(numLettre)); 
	  numLettre =sc.nextInt(); }
	 
	  return this.proposition; 
	  }

	public ArrayList<Lettre> obtenirProposition() {
		 for (int i = 0; i < this.chevalet.size(); i++){
				TextInputDialog dialog = new TextInputDialog("Lettre");
				dialog.setTitle("Placer Lettre");
				dialog.setContentText("Quelle Lettre ?");
				Optional<String> LettrePlacer = dialog.showAndWait();

				TextInputDialog dialog2 = new TextInputDialog("x");
				dialog2.setTitle("Coordonnées");
				dialog2.setContentText("Coordonnée sur x");
				Optional<String> placex = dialog2.showAndWait();
				String cox = placex.get();
				int coorx = Integer.parseInt(cox);
				System.out.println(coorx);

				TextInputDialog dialog3 = new TextInputDialog("y");
				dialog3.setTitle("Coordonnées");
				dialog3.setContentText("Coordonnée sur y");
				Optional<String> placey = dialog3.showAndWait();
				String coy = placey.get();
				int coory = Integer.parseInt(coy);
				System.out.println(coory);
			 if (this.chevalet.get(i).nom.equals(LettrePlacer)){
				 Lettre piece = new Lettre(nom, 6);
				 gp.add(piece, coorx, coory);
 }
		 }

		return this.proposition;
	}

	// Jouer
	public void jouer(Plateau unPlateau, Biblio laBiblio) {
		// Demander une s駻ie de lettres et de positions
		this.proposition = this.placerLettre();
		// M騁hodes qui appellent le plateau
		// V駻ifier si les lettres sont sur la m麥e ligne ou colonne
		int[] valeursCase = unPlateau.memeLigneColonne(this.proposition);
		if (valeursCase != null) {
			// regarder des deux c駸 du mot pour obtenir sa longueur
			unPlateau.longueurMot(valeursCase);
			// V駻ifier que les lettres sont toutes � cot� les unes des autres
			if (unPlateau.motComplet(valeursCase)) {
				// Obtenir le mot et v駻ifier qu'il existe
				if (unPlateau.motNom(valeursCase, laBiblio)) {
					unPlateau.accepterLettres(this.proposition);
					enleverLettreChevalet();

				} else return;
			}
		}
	}

		// Enlever les lettres pos馥s sur le plateau du chevalet
		public void enleverLettreChevalet(){
			for (int i = 0; i < this.proposition.size(); i++){
				for (int k = 0; k < this.chevalet.size(); k++){
					if (this.proposition.get(i).nom.equals(this.chevalet.get(k).nom)){
						this.chevalet.remove(k);
						this.proposition.remove(i);
						break;
					}
				}
			}
		}
	// Piocher
	public void piocher(Pioche unePioche) {
		ArrayList<Lettre> lettresPiochees = unePioche.distribuerLettre(7 - this.chevalet.size());
		this.chevalet.addAll(lettresPiochees);
	}

}
