package application;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Pioche {
	Scanner sc = new Scanner(System.in);

	// Attributs
	String nomPioche;
	ArrayList<Lettre> piocheListe = new ArrayList<>();

	// Constucteur
	public Pioche(String pch) {
		this.nomPioche = pch;
		creationPioche();
	}

	// Méthodes

	// Remplir la pioche
	public void creationPioche() {
		BufferedReader lecteur;
		try { // Ouvre le fichier txt
			lecteur = new BufferedReader(new FileReader("PiocheDebut.txt"));
			for (int k = 0; k < 26; k++) {
				String info = lecteur.readLine();
				String[] tmp = info.split(",");
				String nom = tmp[0];
				String points2 = tmp[1];
				String nombre2 = tmp[2];
				int points = Integer.parseInt(points2);
				int nombre = Integer.parseInt(nombre2);
				for (int i = 0; i < nombre; i++) {
					this.piocheListe.add(new Lettre(nom, points));
				}
			}

			lecteur.close(); // On ferme le fichier avec un catch pour qu'il n'y
								// ait aucun problème
		} catch (IOException e) {
			System.err.println("Erreur de lecture2");
			System.exit(1);
		}

	}

	// Afficher la pioche
	public void afficherPioche() {
		for (int i = 0; i < this.piocheListe.size(); i++) {
			this.piocheListe.get(i).afficherLettre();
		}
	}

	// Distribuer et enlever des lettres de la pioche
	public ArrayList<Lettre> distribuerLettre(int nbrLettre) {
		ArrayList<Lettre> lettreDonnee = new ArrayList<>();
		if (this.piocheListe.size() < nbrLettre) {
			nbrLettre = this.piocheListe.size();
		}
		for (int k = 0; k < nbrLettre; k++) {
			int lettreAuHasard = (int) (Math.random() * (this.piocheListe.size() - 1));
			lettreDonnee.add(this.piocheListe.remove(lettreAuHasard));
		}
		return lettreDonnee;
	}
	/*
	 * // Echanger les lettres public void echangelettres(Lettre[] lettreJeu) {
	 * System.out.print("Quelles lettres voulez-vous échanger? "); // Présenter
	 * les lettres à l'utilisateur for (int i = 0; i < 6; i++) {
	 * System.out.print(lettreJeu[i].nom + " "); }
	 * System.out.println(lettreJeu[6].nom); System.out.println(
	 * "Ecrivez <<ok>> quand vous avez fini de saisir."); // L'utilisateur
	 * saisit ces lettres String nomLettre = " "; while (nomLettre.equals("ok")
	 * != true) { nomLettre = sc.nextLine(); // Trouver une lettre en fonction
	 * de son nom for (int j = 0; j < 7; j++) { if (lettreJeu[j] != null &&
	 * nomLettre.equals(lettreJeu[j].nom)) { piocheListe.add(lettreJeu[j]);
	 * lettreJeu[j] = null; break; } } } }
	 */
}
