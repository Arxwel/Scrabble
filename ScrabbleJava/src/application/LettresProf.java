package application;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class LettresProf extends Group {
    static final int SIZE = 31;
	private int nbPoints;
	private String lettre;

    LettresProf(String lettre, int nbPoints) {
        this.lettre = lettre;
        this.nbPoints = nbPoints;

        // Forme de la pièce
        Rectangle rect = new Rectangle(31,31);
        rect.setFill(Color.YELLOW);
        rect.setArcHeight(5);
        rect.setArcWidth(5);
        rect.setStrokeType(StrokeType.INSIDE);
        rect.setStroke(Color.BLACK);
        rect.setStrokeWidth(1.5);
        getChildren().add(rect);

        // Lettre
        Text lettreTxt = new Text(lettre);
        lettreTxt.setFont(new Font(14));
        lettreTxt.setX(13);
        lettreTxt.setY(19);
        getChildren().add(lettreTxt);

        // Points
        Text ptTxt = new Text(""+nbPoints);
        ptTxt.setFont(new Font(8));
        ptTxt.setX(20);
        ptTxt.setY(27);
        getChildren().add(ptTxt);
    }
}