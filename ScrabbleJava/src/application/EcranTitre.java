/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package application;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Romai
 */
public class EcranTitre extends JPanel {
    private JFrame window ;
    private JPanel mainPanel;
    private JButton jouer;
    private JButton quitter;
    private JPanel buttPanel;
    private JLabel labPseudo;
    private JTextField txtPseudo;

      public EcranTitre() {
        window = new JFrame();
        buttPanel = new JPanel(new GridLayout(1,6));
        window.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        window.setSize(800, 600);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        window.setLocation(dim.width/2-window.getSize().width/2, dim.height/2-window.getSize().height/2);
        window.setTitle("Inscription");
        mainPanel = new JPanel(new BorderLayout());
        jouer = new JButton("Jouer");
        quitter = new JButton("Quitter");
        labPseudo = new JLabel("Psuedo :");
        txtPseudo = new JTextField("Joueur");
        mainPanel.add(labPseudo);
        mainPanel.add(txtPseudo);
        jouer.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
            });
        quitter.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
            });
        mainPanel.add(jouer, BorderLayout.SOUTH);
        mainPanel.add(quitter, BorderLayout.SOUTH);
        buttPanel.add(new JPanel());
        buttPanel.add(jouer);
        buttPanel.add(new JPanel());
        buttPanel.add(new JPanel());
        buttPanel.add(quitter);
        buttPanel.add(new JPanel());
        mainPanel.add(buttPanel,BorderLayout.SOUTH);
        window.add(mainPanel);
        window.setVisible(true);
        
    }
}

