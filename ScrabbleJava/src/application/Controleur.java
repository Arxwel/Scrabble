package application;

import java.io.IOException;
import java.util.Optional;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Controleur {
	public void Quitter(ActionEvent e) {
		System.exit(0);
	}
	String pseudonyme = "Joueur";

//	public void setPseudo(String pseudos) {
//		pseudonyme = pseudos;
//	}

	public void Pseudo() {
		//Fenère où l'utilisateur peut choisir un pseudo qui sera affiché à coté du plateau
		TextInputDialog dialog = new TextInputDialog("Joueur 1");
		dialog.setTitle("Pseudo");
		dialog.setContentText("Choisissez un Pseudo :");
		Optional<String> pseudo = dialog.showAndWait();
		if (pseudo.isPresent()) {
			pseudonyme = pseudo.get();
		}
	}

	Stage primaryStage;

	void setPrimaryStage(Stage stage) {
		primaryStage = stage;
	}

	public void Commencer(ActionEvent e) {
		Pane root; //Création du Pane plateau
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Plateau.fxml"));
			root = (Pane) loader.load();
			Scene scene = new Scene(root, 700, 500, Color.LIGHTGREEN);
                        Pseudo();
			Controleur2 controleur2 = loader.getController();
			controleur2.setPseudo(pseudonyme);
			primaryStage.setScene(scene);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
}