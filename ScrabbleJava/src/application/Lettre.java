package application;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Lettre extends Group {
	// Attributs
	String nom;
	int points;
	int posLigne;
	int posColonne;
	static final int SIZE = 31;
	
	// Constructeur
	public Lettre(String nm, int pts) {
		this.nom = nm;
		this.points = pts;
		// Forme de la pièce
        Rectangle rect = new Rectangle(31,31);
        rect.setFill(Color.YELLOW);
        rect.setArcHeight(5);
        rect.setArcWidth(5);
        rect.setStrokeType(StrokeType.INSIDE);
        rect.setStroke(Color.BLACK);
        rect.setStrokeWidth(1.5);
        getChildren().add(rect);

        // Lettre
        Text lettreTxt = new Text(nom);
        lettreTxt.setFont(new Font(14));
        lettreTxt.setX(13);
        lettreTxt.setY(19);
        getChildren().add(lettreTxt);

        // Points
        Text ptTxt = new Text(""+points);
        ptTxt.setFont(new Font(8));
        ptTxt.setX(20);
        ptTxt.setY(27);
        getChildren().add(ptTxt);
	}

	// Méthodes

	// Afficher une lettre
	public void afficherLettre() {
		System.out.println("La lettre est " + this.nom + ". Elle vaut " + this.points + " point(s).");
	}

	// Afficher lettres et places sur le plateau
	public void afficherLettreSurPlateau(){
		System.out.println("Lettre : "+this.nom+". Ligne :"+this.posLigne+". Colonne : "+this.posColonne+".");
	}
}
